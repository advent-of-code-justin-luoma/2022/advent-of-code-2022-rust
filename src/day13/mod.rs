use serde::{Deserialize, Serialize};

use crate::read_file;

#[derive(Debug, Deserialize, Serialize)]
#[serde(untagged)]
enum Item {
    Int(i32),
    List(Vec<Item>),
}

pub fn part1() {
    let parsed = parse("./inputs/day13");
    let valid = solve_p1(&parsed);
    let sum: u32 = valid.iter().map(|(n, _, _)| *n as u32).sum();
    println!("sum = {:?}", sum);
}

pub fn example() {
    // let input = "[1,[2,[3,[4,[5,6,7]]]],8,9]";

    // let list: Vec<Item> = serde_json::from_str(input).unwrap();

    // println!("{:?}", list);

    let parsed = parse("./inputs/day13example");

    // parsed.iter().for_each(|pair| {
    //     println!("pair = {:?}", pair);
    // });

    // let item = &parsed[6][1];

    // let list = len(item);

    // println!("list = {:?}", list);

    let valid = solve_p1(&parsed);

    // valid.iter().for_each(|(n, left, right)| {
    //     println!("n = {:?}\n{:?}\n{:?}", n, left, right);
    // });

    let sum: u32 = valid.iter().map(|(n, _, _)| *n as u32).sum();

    println!("sum = {:?}", sum);
}

fn solve_p1<'l>(pairs: &'l Vec<Vec<Item>>) -> Vec<(usize, &'l Item, &'l Item)> {
    let mut valid = Vec::new();

    for (n, pair) in pairs.iter().enumerate() {
        let n = n + 1;
        let left = &pair[0];
        let right = &pair[1];

        let left_list = to_list(left);
        let right_list = to_list(right);

        if left_list.is_empty() && right_list.is_empty() {
            if len(left) < len(right) {
                valid.push((n, left, right));
            }

            continue;
        }

        if left_list.is_empty() && right_list.len() > 0 {
            valid.push((n, left, right));
            continue;
        }

        for i in 0..left_list.len() {
            let l = left_list.get(i);
            let r = right_list.get(i);

            if l.is_none() && r.is_some() {
                valid.push((n, left, right));
                break;
            }

            if l.is_some() && r.is_none() {
                break;
            }

            if l.is_some() && r.is_some() {
                let l = l.unwrap();
                let r = r.unwrap();

                if l > r {
                    break;
                }

                if l < r {
                    valid.push((n, left, right));
                    break;
                }
            }

            if i == left_list.len() - 1 && right_list.len() > left_list.len() {
                valid.push((n, left, right));
            }
        }
    }

    valid
}

fn len(item: &Item) -> usize {
    let mut size = 0;
    if let Item::List(l) = item {
        size += l.len();
        for l in l.iter() {
            size += len(l);
        }
    }

    size
}

fn to_list(item: &Item) -> Vec<i32> {
    let mut list = Vec::new();

    match item {
        Item::Int(i) => list.push(*i),
        Item::List(l) => {
            for i in l.iter() {
                let mut l = to_list(i);
                list.append(&mut l);
            }
        }
    }

    list
}

// fn compare(left: Item, right: Item) -> bool {
//     if let Item::List(mut left_l) = left {
//         if let Item::List(mut right_l) = right {
//             let left = left_l.pop_front();
//             let right = right_l.pop_front();
//             if left.is_none() && right.is_some() {
//                 return true;
//             } else if left.is_some() && right.is_none() {
//                 return false;
//             }
//             if left.is_some() && right.is_some() {
//                 let left = left.unwrap();
//                 let right = right.unwrap();
//                 if compare(left, right) {
//                     return compare_list(left_l, right_l);
//                 }
//             }
//         }
//     }

//     false
// }

fn compare_list(left: Vec<i32>, right: Vec<i32>) -> bool {
    let mut valid = false;

    for i in 0..left.len() {
        let left = left.get(i);
        let right = right.get(i);
        if left.is_some() && right.is_none() {
            return false;
        }
        if left.is_none() && right.is_some() {
            return true;
        }
        if left.is_some() && right.is_some() {
            let left = left.unwrap();
            let right = right.unwrap();

            if left < right {
                return true;
            }

            valid = left == right;
        }
        if !valid {
            break;
        }
    }

    valid
}

fn parse(file: &str) -> Vec<Vec<Item>> {
    let file = read_file(file);

    file.split("\r\n\r\n")
        .map(|pair| {
            pair.split_whitespace()
                .map(|s| serde_json::from_str(s).unwrap())
                .collect::<Vec<Item>>()
        })
        .collect::<Vec<Vec<Item>>>()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_compare_list_right_order_left_shorter() {
        let left = vec![4, 4, 4, 4];
        let right = vec![4, 4, 4, 4, 4];

        assert!(compare_list(left, right));
    }

    #[test]
    fn test_compare_list_wrong_right_shorter() {
        let left = vec![0; 3];
        let right = vec![0; 2];

        assert!(!compare_list(left, right));
    }

    #[test]
    fn test_compare_list_right_order_left_is_less() {
        let left = vec![1, 1, 3, 1, 1];
        let right = vec![1, 1, 5, 1, 1];

        assert!(compare_list(left, right));
    }
}
