use std::collections::HashMap;

use crate::read_file;

#[derive(Debug, Clone)]
struct Directory {
    name: String,
    parent: Option<String>,
    subdirectories: Vec<String>,
    files: Vec<File>,
}

#[derive(Debug, Clone)]
struct File {
    name: String,
    size: usize,
}

impl Directory {
    fn new(name: String, parent: Option<String>) -> Self {
        Self {
            name,
            parent,
            subdirectories: Vec::new(),
            files: Vec::new(),
        }
    }
}

impl File {
    fn new(name: String, size: usize) -> Self {
        Self { name, size }
    }
}

pub fn part1() {
    let parsed = parse("./inputs/day7");

    solve(parsed);
}

pub fn example() {
    let parsed = parse("./inputs/day7example");

    solve(parsed);
}

fn solve(parsed: Vec<Vec<String>>) {
    let mut directories = HashMap::new();
    // let mut files = Vec::new();

    let name = String::from("/");
    let root = Directory::new(name.clone(), None);
    directories.insert(name, root.clone());

    let mut current_directory = root.clone();

    for ins in parsed.iter() {
        // println!("{:?}", ins);
        let cmd = ins[0].clone();
        match cmd.as_str() {
            "ls" => {
                let mut ins = ins.clone();
                ins.remove(0);
                for ls in ins.iter() {
                    let parts = ls.split_whitespace().collect::<Vec<&str>>();
                    let dir_size = parts[0];
                    match dir_size {
                        "dir" => {
                            let dir = parts[1];
                            let parent = current_directory.clone().name;
                            if !directories.contains_key(dir) {
                                let directory =
                                    Directory::new(dir.to_string(), Some(parent.clone()));
                                directories.insert(dir.to_string(), directory.clone());
                            }
                            if !current_directory.subdirectories.contains(&dir.to_string()) {
                                let mut updated = current_directory.clone();
                                updated.subdirectories.push(dir.to_string());
                                current_directory = updated.clone();
                                directories.insert(parent, updated);
                            }
                        }
                        _ => {
                            let file = parts[1].to_string();
                            let size: usize = dir_size.parse().unwrap();
                            let file = File::new(file, size);
                            if current_directory.name == "/" {
                                current_directory.files.push(file);
                                directories.insert("/".to_string(), current_directory.clone());
                            } else {
                                directories
                                    .entry(current_directory.name.clone())
                                    .and_modify(|dir| dir.files.push(file));
                            }
                        }
                    }
                }
            }
            _ => {
                let parts = cmd.split_whitespace().collect::<Vec<&str>>();
                let dir = parts[1].clone();
                match dir {
                    "/" => {
                        current_directory = root.clone();
                    }
                    ".." => {
                        if current_directory.name != "/" {
                            let parent = current_directory.clone().parent.unwrap();
                            current_directory = directories.get(&parent).unwrap().clone();
                        }
                    }
                    _ => {
                        let parent = current_directory.clone().name;
                        if !current_directory.subdirectories.contains(&dir.to_string()) {
                            let mut updated = current_directory.clone();
                            updated.subdirectories.push(dir.to_string());
                            directories.insert(parent.clone(), updated);
                        }
                        if let Some(directory) = directories.get(dir) {
                            current_directory = directory.clone();
                        } else {
                            let directory = Directory::new(dir.to_string(), Some(parent));
                            directories.insert(dir.to_string(), directory.clone());
                            current_directory = directory;
                        }
                    }
                }
            }
        }
    }

    // directories
    //     .iter()
    //     .for_each(|(k, v)| println!("{}: {:?}", k, v));

    let mut solution: Vec<usize> = Vec::new();

    for dir in directories.iter() {
        let size = dir_size(&directories, dir.0);
        if size <= 100000 {
            solution.push(size);
        }
    }

    println!("{:?}", solution.iter().sum::<usize>());
}

fn dir_size(directories: &HashMap<String, Directory>, directory: &str) -> usize {
    let directory = directories.get(directory).unwrap();

    let mut size = 0;

    for file in directory.files.iter() {
        size += file.size;
    }

    for sub_dir in directory.subdirectories.iter() {
        size += dir_size(directories, sub_dir);
    }

    size
}

fn parse(file: &str) -> Vec<Vec<String>> {
    let input = read_file(file);

    let mut parsed = input
        .split("$ ")
        .map(|s| s.trim().to_string())
        .collect::<Vec<String>>();
    parsed.remove(0);

    parsed
        .iter()
        .map(|s| {
            s.split("\r\n")
                .map(|s| s.to_string())
                .collect::<Vec<String>>()
        })
        .collect()
}

/* fn parse_input(file: &str) {
    let lines = read_lines(file);
    let mut directories = HashMap::new();
    // let mut files = Vec::new();

    let name = String::from("/");
    let root = Directory::new(name.clone(), None);
    directories.insert(name, root.clone());

    let mut current_directory = root.clone();

    for line in lines {
        let line = line.unwrap();
        let parts: Vec<&str> = line.split(' ').collect();
        if parts.first().unwrap() == &"$" {
            let command = parts[1];
            match command {
                "cd" => {
                    let dir = parts[2];
                    match dir {
                        "/" => {
                            current_directory = root.clone();
                        }
                        ".." => {
                            let parent = current_directory.clone().parent.unwrap();
                            current_directory = directories.get(&parent).unwrap().clone();
                        }
                        _ => {
                            if let Some(directory) = directories.get(dir) {
                                current_directory = directory.clone();
                            } else {
                                let parent = current_directory.clone().parent.unwrap();
                                let directory = Directory::new(dir.to_string(), Some(parent));
                                directories.insert(dir.to_string(), directory);
                            }
                        }
                    }
                }
                "ls" => {}
                _ => unreachable!(),
            }
        }
    }
} */
