use std::collections::{HashMap, HashSet};

use crate::read_lines;

const X_MIN: u32 = 445;
const X_MAX: u32 = 515;
const Y_MAX: u32 = 170;
const SAND_X: u32 = 500;

const ROCK: char = '#';
const SAND: char = 'o';
const FLOOR: u32 = 165;

#[derive(Debug)]
struct Grid {
    grid: HashMap<(u32, u32), char>,
    part2: bool,
}

impl Grid {
    pub fn new(mut rock_coords: HashMap<(u32, u32), char>, part2: bool) -> Self {
        if part2 {
            for x in X_MIN..X_MAX {
                rock_coords.insert((x, 165), '#');
            }
        }
        Self {
            grid: rock_coords,
            part2,
        }
    }

    pub fn produce_sand(&mut self) -> bool {
        let x = SAND_X;
        self.next_sand(x, 0)
    }

    pub fn count_sand(&self) -> usize {
        self.grid.values().filter(|space| space == &&SAND).count()
    }

    fn next_sand(&mut self, x: u32, y: u32) -> bool {
        // println!("{x}, {y}");
        for y in y..Y_MAX {
            let space = self.grid.get(&(x, y));

            if x == SAND_X && y == 0 {
                if space.is_some() {
                    break;
                }
            }
            // println!("y = {:?}", y);
            if self.grid.contains_key(&(x, y)) || (self.part2 && y >= FLOOR) {
                // if (y == 0 && x == SAND_X) && space == &'+' {
                // check left (we are already "down")
                if self.grid.contains_key(&(x - 1, y)) || (self.part2 && y >= FLOOR) {
                    // check right
                    if self.grid.contains_key(&(x + 1, y)) || (self.part2 && y >= FLOOR) {
                        self.grid.insert((x, y - 1), SAND);
                        return true;
                    } else {
                        // right is empty
                        return self.next_sand(x + 1, y);
                    }
                } else {
                    // left is empty
                    return self.next_sand(x - 1, y);
                }
            }
            // }
        }
        false
    }

    pub fn print(&self) {
        for y in 0..Y_MAX {
            print!("{y}\t");
            for x in X_MIN..X_MAX {
                if let Some(space) = self.grid.get(&(x, y)) {
                    print!("{space}");
                } else {
                    print!(".");
                }
            }
            println!();
        }
    }
}

pub fn part1() {
    let points = parse("./inputs/day14");
    let fill_coords = get_fill_coords(points);

    let mut grid = Grid::new(fill_coords, false);

    while grid.produce_sand() {}

    let count = grid.count_sand();

    println!("{count}");

    grid.print();
}

pub fn part2() {
    let points = parse("./inputs/day14");
    let fill_coords = get_fill_coords(points);

    let mut grid = Grid::new(fill_coords, true);

    while grid.produce_sand() {}

    let count = grid.count_sand();

    println!("{count}");

    // grid.print();
}

fn get_fill_coords(points: Vec<Vec<(u32, u32)>>) -> HashMap<(u32, u32), char> {
    points
        .iter()
        .flat_map(|points| {
            points
                .windows(2)
                .map(|line| (line[0], line[1]))
                .flat_map(|((x1, y1), (x2, y2))| {
                    let mut coords = HashSet::new();
                    if x1 < x2 {
                        for x in x1..=x2 {
                            coords.insert((x, y1));
                        }
                    } else if x2 < x1 {
                        for x in x2..=x1 {
                            coords.insert((x, y1));
                        }
                    } else if y1 < y2 {
                        for y in y1..=y2 {
                            coords.insert((x1, y));
                        }
                    } else if y2 < y1 {
                        for y in y2..=y1 {
                            coords.insert((x1, y));
                        }
                    }

                    coords
                })
                .map(|coords| (coords, '#'))
        })
        .collect()
}

fn parse(file: &str) -> Vec<Vec<(u32, u32)>> {
    read_lines(file)
        .map(|line| line.unwrap())
        .map(|line| {
            line.trim()
                .split(" -> ")
                .map(|xy_string| {
                    xy_string
                        .split(',')
                        .map(|xy| xy.parse::<u32>().unwrap())
                        .collect::<Vec<_>>()
                })
                .map(|xys| (xys[0], xys[1]))
                .collect()
        })
        .collect()
}

fn min_max(xys: &Vec<(u32, u32)>) -> (u32, u32, u32, u32) {
    let mut x_min = u32::MAX;
    let mut y_min = u32::MAX;
    let mut x_max = u32::MIN;
    let mut y_max = u32::MIN;

    xys.iter().for_each(|&(x, y)| {
        if x < x_min {
            x_min = x;
        }
        if y < y_min {
            y_min = y;
        }
        if x > x_max {
            x_max = x;
        }
        if y > y_max {
            y_max = y;
        }
    });

    (x_min, y_min, x_max, y_max)
}
