use std::collections::VecDeque;

use lazy_static::lazy_static;
use regex::Regex;

use crate::read_lines;

const CYCLES: [u64; 6] = [20, 60, 100, 140, 180, 220];

#[derive(Debug)]
struct Clock {
    cycle: u64,
    register: i64,
    instructions: VecDeque<Instruction>,
    executing: bool,
}

impl Clock {
    fn new(instructions: VecDeque<Instruction>) -> Self {
        Self {
            cycle: 1,
            register: 1,
            instructions,
            executing: false,
        }
    }

    fn tick(&mut self) -> bool {
        if self.instructions.is_empty() {
            return false;
        }

        if let Some(Instruction::Addx(x)) = self.instructions.front() {
            if self.executing {
                self.register += x;
                self.instructions.pop_front();
                self.executing = false;
                self.cycle += 1;
                return true;
            } else {
                self.executing = true;
                self.cycle += 1;
                return true;
            }
        }

        if let Some(Instruction::Noop) = self.instructions.front() {
            self.instructions.pop_front();
            self.executing = false;
            self.cycle += 1;
            return true;
        }

        false
    }
}

#[derive(Debug)]
enum Instruction {
    Noop,
    Addx(i64),
}

pub fn part1() {
    let instructions = parse("./inputs/day10");

    let mut clock = Clock::new(instructions);

    solve(&mut clock);
}

pub fn example() {
    let instructions = parse("./inputs/day10example");

    let mut clock = Clock::new(instructions);

    solve(&mut clock);
}

pub fn part2() {
    let instructions = parse("./inputs/day10");

    let mut clock = Clock::new(instructions);

    let mut screen = Vec::new();

    let mut line = String::new();

    let mut cursor = 0;

    loop {
        let len = line.len();
        if len == 40 {
            screen.push(line.clone());
            line.clear();
            cursor = 0;
        }

        let register = clock.register;

        if register == cursor || register == cursor - 1  || register == cursor + 1 {
            line.push('#');
        } else {
            line.push('.');
        }

        if !clock.tick() {
            break;
        }

        cursor += 1;
    }

    for line in screen.iter() {
        println!("{}", line);
    }
}

fn solve(clock: &mut Clock) {
    let mut cycles = Vec::new();

    loop {
        if !clock.tick() {
            break;
        }
        if CYCLES.contains(&clock.cycle) {
            let cycle = clock.cycle;
            let register = clock.register;
            let total = register * cycle as i64;
            cycles.push(total);
            println!("{}", total);
        }
    }

    println!("{}", cycles.iter().sum::<i64>());
}

fn parse(file: &str) -> VecDeque<Instruction> {
    let mut instructions = VecDeque::new();

    lazy_static! {
        static ref RE: Regex = Regex::new(r"(-?\d+)").unwrap();
    }

    for line in read_lines(file) {
        let line = line.unwrap();
        if let Some(cap) = RE.captures(&line) {
            let x: i64 = cap.get(1).unwrap().as_str().parse().unwrap();
            instructions.push_back(Instruction::Addx(x));
        } else {
            instructions.push_back(Instruction::Noop);
        }
    }

    instructions
}
