use std::collections::{HashMap, HashSet};

use lazy_static::lazy_static;
use regex::Regex;

use crate::read_lines;

pub fn part1() {
    let (sensors, beacons) = parse("./inputs/day15");

    let mut covered = HashSet::new();

    for i in 0..sensors.len() {
        let (x, y) = sensors[i];
        let (bx, by) = beacons[i];
        let distance = distance((x, y), (bx, by)) as i32;
        if (y + distance >= 2000000 && y <= 2000000) || (y - distance <= 2000000 && y >= 2000000) {
            let offset = y.abs_diff(2000000) as i32;
            let covered_left = x - (distance - offset);
            let coverd_right = x + (distance - offset);

            for x in covered_left..=coverd_right {
                if !beacons.contains(&(x, 2000000)) {
                    covered.insert(x);
                }
            }
        }
    }

    println!("covered = {:?}", covered.len());
}

#[derive(Debug, Clone)]
struct Sensor {
    pos: (i32, i32),
    beacon: (i32, i32),
    distance: i32,
    range: HashMap<i32, (i32, i32)>,
}

pub fn part2() {
    let sensors = build_sensors("./inputs/day15example");

    for y in 0..=20 {
        let mut ranges = Vec::new();

        let mut range = (-20, -20);

        for sensor in sensors.iter() {
            if let Some(range) = sensor.range.get(&y) {
                ranges.push(range);
            }
        }

        ranges.sort_by(|(start1, _), (start2, _)| start1.cmp(start2));

        for window in ranges.windows(2) {
            // println!("{:?}", window);

            if range == (-20, -20) {
                if let Some(updated) = merge_ranges(*window[0], *window[1]) {
                    range = updated;
                } else {
                    println!("no range: {:?}", window);
                }
            }

            if window.len() == 2 {
                if let Some(_updated) = merge_ranges(*window[0], *window[1]) {
                    // range = merge_ranges(range, updated).un;
                }
            } else if window.len() == 1 {
                if let Some(updated) = merge_ranges(range, *window[0]) {
                    range = updated;
                }
            }
        }

        let (start, end) = range;
        if !(start <= 0 && end >= 20) {
            println!("{y}: {:?}", range);
        }
    }

    // println!("{:?}", sensors);
    // for search_y in 2000000..=4000000 {
    //     let mut covered: HashSet<i32> = (0..=4000000).collect();
    //     for i in 0..sensors.len() {
    //         let (x, y) = sensors[i];
    //         let (bx, by) = beacons[i];
    //         let distance = distance((x, y), (bx, by)) as i32;
    //         if (y + distance >= search_y && y <= search_y)
    //             || (y - distance <= search_y && y >= search_y)
    //         {
    //             let offset = y.abs_diff(search_y) as i32;
    //             let covered_left = x - (distance - offset);
    //             let coverd_right = x + (distance - offset);

    //             for x in covered_left..=coverd_right {
    //                 covered.remove(&x);
    //             }
    //         }
    //     }
    //     if covered.len() > 0 {
    //         println!("{}: {:?}", search_y, covered);
    //     }
    // }
}

fn merge_ranges((start1, end1): (i32, i32), (start2, end2): (i32, i32)) -> Option<(i32, i32)> {
    // start1 first
    if start1 <= start2 && start1 <= end2 {
        // end1 last
        if end1 >= start2 && end1 >= end2 && start2 <= end1 {
            return Some((start1, end1));
        }

        // end2 last
        if end2 > end1 && start2 <= end1 {
            return Some((start1, end2));
        }
    }

    // start2 first
    if start2 <= start1 && start2 <= end1 {
        // end2 last
        if end2 >= start1 && end2 >= end1 && start1 <= end2 {
            return Some((start2, end2));
        }

        // end1 last
        if end1 > end2 && start1 <= end2 {
            return Some((start2, end1));
        }
    }

    None
}

pub fn example() {
    let (sensors, beacons) = parse("./inputs/day15example");

    let mut covered = HashSet::new();

    for i in 0..sensors.len() {
        let (x, y) = sensors[i];
        let (bx, by) = beacons[i];
        let distance = distance((x, y), (bx, by)) as i32;
        if (y + distance >= 10 && y <= 10) || (y - distance <= 10 && y >= 10) {
            let offset = y.abs_diff(10) as i32;
            let covered_left = x - (distance - offset);
            let coverd_right = x + (distance - offset);

            for x in covered_left..=coverd_right {
                if !beacons.contains(&(x, 10)) {
                    covered.insert(x);
                }
            }
        }
    }

    println!("covered = {:?}", covered.len());
}

pub fn example_p2() {
    let (sensors, beacons) = parse("./inputs/day15example");

    let mut covered = Vec::new();

    for i in 0..=20 {
        covered.push(i);
    }

    for search_y in 0..=20 {
        let mut covered: HashSet<&i32> = HashSet::from_iter(covered.iter());
        for i in 0..sensors.len() {
            let (x, y) = sensors[i];
            let (bx, by) = beacons[i];
            let distance = distance((x, y), (bx, by)) as i32;
            if (y + distance >= search_y && y <= search_y)
                || (y - distance <= search_y && y >= search_y)
            {
                let offset = y.abs_diff(search_y) as i32;
                let covered_left = x - (distance - offset);
                let coverd_right = x + (distance - offset);

                for x in covered_left..=coverd_right {
                    covered.remove(&x);
                }
            }
        }
        if covered.len() > 0 {
            println!("{}: {:?}", search_y, covered);
        }
    }
}

fn build_sensors(file: &str) -> Vec<Sensor> {
    let (sensors, beacons) = parse(file);

    let mut s = Vec::new();

    for i in 0..sensors.len() {
        let (x, y) = sensors[i];
        let (bx, by) = beacons[i];
        let distance = distance((x, y), (bx, by)) as i32;

        let mut range = HashMap::new();

        for row in (y - distance)..=(y + distance) {
            let offset = y.abs_diff(row) as i32;
            let covered_left = x - (distance - offset);
            let coverd_right = x + (distance - offset);
            range.insert(row, (covered_left, coverd_right));
        }

        let sensor = Sensor {
            pos: (x, y),
            beacon: (bx, by),
            distance,
            range,
        };

        s.push(sensor);
    }

    s
}

fn distance((x1, y1): (i32, i32), (x2, y2): (i32, i32)) -> u32 {
    x1.abs_diff(x2) + y1.abs_diff(y2)
}

fn parse(file: &str) -> (Vec<(i32, i32)>, Vec<(i32, i32)>) {
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)"
        )
        .unwrap();
    }

    let mut sensors = Vec::new();
    let mut beacons = Vec::new();

    for line in read_lines(file) {
        let line = line.unwrap();

        let caps = RE.captures(&line).unwrap();

        let sensor_x: i32 = caps.get(1).unwrap().as_str().parse().unwrap();
        let sensor_y: i32 = caps.get(2).unwrap().as_str().parse().unwrap();
        let beacon_x: i32 = caps.get(3).unwrap().as_str().parse().unwrap();
        let beacon_y: i32 = caps.get(4).unwrap().as_str().parse().unwrap();

        sensors.push((sensor_x, sensor_y));
        beacons.push((beacon_x, beacon_y));
    }

    (sensors, beacons)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_merge_ranges() {
        assert_eq!(merge_ranges((-8, 12), (6, 10)), Some((-8, 12)));

        assert_eq!(merge_ranges((0, 10), (9, 15)), Some((0, 15)));

        assert_eq!(merge_ranges((0, 10), (10, 15)), Some((0, 15)));

        assert_eq!(merge_ranges((0, 9), (9, 15)), Some((0, 15)));

        assert_eq!(merge_ranges((9, 15), (0, 10)), Some((0, 15)));

        assert_eq!(merge_ranges((10, 15), (0, 10)), Some((0, 15)));

        assert_eq!(merge_ranges((0, 10), (10, 10)), Some((0, 10)));

        assert_eq!(merge_ranges((10, 10), (0, 10)), Some((0, 10)));

        assert_eq!(merge_ranges((8, 9), (0, 10)), Some((0, 10)));

        assert_eq!(merge_ranges((0, 10), (5, 6)), Some((0, 10)));

        assert_eq!(merge_ranges((11, 15), (0, 10)), None);
    }
}
