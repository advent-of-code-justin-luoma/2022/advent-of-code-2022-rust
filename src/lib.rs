#![allow(dead_code)]
#![feature(int_roundings)]

use std::{fs::{File, self}, io::{self, Lines, BufReader, BufRead}, time::Instant};

mod day1;
mod day7;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;

pub fn run() {
    let now = Instant::now();

    day15::part2();

    println!("Time: {:#?}", now.elapsed());
}

fn read_file(file: &str) -> String {
    fs::read_to_string(file).expect("Couldn't read file")
}

fn read_lines(file: &str) -> Lines<BufReader<File>> {
    let file = File::open(file).expect("Couldn't open file");

    io::BufReader::new(file).lines()
}