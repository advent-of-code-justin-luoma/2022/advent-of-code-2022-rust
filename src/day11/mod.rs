mod solve;

use std::collections::VecDeque;

use lazy_static::lazy_static;
use num_bigint::BigInt;
use regex::Regex;

use crate::read_file;

const BORED: u128 = 3;

pub fn solve() {
    solve::main();
}

#[derive(Debug, Clone)]
struct Test(BigInt, usize, usize);

#[derive(Debug)]
struct Monkey {
    inspects: usize,
    items: VecDeque<BigInt>,
    operation: Operation,
    test: Test,
}

impl Monkey {
    fn new(items: VecDeque<BigInt>, operation: Operation, test: Test) -> Self {
        Self {
            inspects: 0,
            items,
            operation,
            test,
        }
    }

    // fn inspect(&mut self) -> Vec<(usize, u128)> {
    //     let mut throws = Vec::new();

    //     loop {
    //         if self.items.is_empty() {
    //             break;
    //         }
    //         let item = self.items.pop_front().unwrap();
    //         let inspected = match self.operation {
    //             Operation::Add(n) => item + n,
    //             Operation::Multiply(n) => item * n,
    //             Operation::OldMultiply => item * item,
    //         };
    //         let bored = inspected.div_floor(BORED);
    //         let monkey = Monkey::test(bored, self.test);

    //         throws.push((monkey, bored));
    //         self.inspects += 1;
    //     }

    //     throws
    // }

    fn inspect_p2(&mut self) -> Vec<(usize, BigInt)> {
        let mut throws = Vec::new();

        loop {
            if self.items.is_empty() {
                break;
            }
            let item = self.items.pop_front().unwrap();
            let inspected = match self.operation {
                Operation::Add(n) => item + n,
                Operation::Multiply(n) => item * n,
                Operation::OldMultiply => item.clone() * item,
            };
            // let bored = inspected.div_floor(BORED);
            let monkey = Monkey::test(inspected.clone(), self.test.clone());

            throws.push((monkey, inspected));
            self.inspects += 1;
        }

        throws
    }

    fn test(value: BigInt, test: Test) -> usize {
        if value % test.0 == BigInt::from(0) {
            test.1
        } else {
            test.2
        }
    }

    fn parse(file: &str) -> Vec<Self> {
        lazy_static! {
            static ref OPERATION: Regex =
                Regex::new(r"old\s(?P<operator>[+*])\s(?P<operand>\d+|old)").unwrap();
            static ref TEST: Regex = Regex::new(r"(\d+)").unwrap();
        }

        let file = read_file(file);

        let split: Vec<String> = file.split("\r\n\r\n").map(|s| s.to_string()).collect();

        let mut monkeys = Vec::new();

        // let split: Vec<Vec<&str>> =
        for monkey in split
            .iter()
            .map(|s| s.split("\r\n").map(|s| s.trim()).collect::<Vec<&str>>())
            .into_iter()
        {
            let items: VecDeque<BigInt> = monkey[1]
                .split_at(16)
                .1
                .split(", ")
                .map(|n| n.parse().unwrap())
                .collect();

            let operation = monkey[2];
            let operation = OPERATION.captures(operation).unwrap();
            let operation = match operation.name("operand").unwrap().as_str() {
                "old" => Operation::OldMultiply,
                num => match operation.name("operator").unwrap().as_str() {
                    "*" => Operation::Multiply(num.parse().unwrap()),
                    "+" => Operation::Add(num.parse().unwrap()),
                    _ => unreachable!(),
                },
            };

            let test = monkey[3];
            let test_value: BigInt = TEST
                .captures(test)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse()
                .unwrap();

            let test_true = monkey[4];
            let test_true: usize = TEST
                .captures(test_true)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse()
                .unwrap();

            let test_false = monkey[5];
            let test_false: usize = TEST
                .captures(test_false)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse()
                .unwrap();

            monkeys.push(Monkey::new(
                items,
                operation,
                Test(test_value, test_true, test_false),
            ));
        }

        monkeys
    }
}

#[derive(Debug, Clone, Copy)]
enum Operation {
    Add(u128),
    Multiply(u128),
    OldMultiply,
}

// pub fn part1() {
//     let mut monkeys = Monkey::parse("./inputs/day11");

//     for _ in 0..20 {
//         round(&mut monkeys);
//     }

//     monkeys.sort_by(|m1, m2| m1.inspects.cmp(&m2.inspects));

//     let one = monkeys.pop().unwrap();

//     let two = monkeys.pop().unwrap();

//     println!("{:?}", monkeys);

//     println!("{}", one.inspects * two.inspects);
// }

// pub fn example() {
//     let mut monkeys = Monkey::parse("./inputs/day11example");

//     for _ in 0..20 {
//         round(&mut monkeys);
//     }

//     monkeys.sort_by(|m1, m2| m1.inspects.cmp(&m2.inspects));

//     let one = monkeys.pop().unwrap();

//     let two = monkeys.pop().unwrap();

//     println!("{:?}", monkeys);

//     println!("{}", one.inspects * two.inspects);
// }

pub fn example_p2() {
    let mut monkeys = Monkey::parse("./inputs/day11example");

    for _ in 0..10000 {
        round_p2(&mut monkeys);
    }

    monkeys.sort_by(|m1, m2| m1.inspects.cmp(&m2.inspects));

    let one = monkeys.pop().unwrap();

    let two = monkeys.pop().unwrap();

    println!("{:?}", monkeys);

    println!("{}", one.inspects * two.inspects);
}

// fn round(monkeys: &mut Vec<Monkey>) {
//     for i in 0..monkeys.len() {
//         let monkey = monkeys.get_mut(i).unwrap();
//         let throws = monkey.inspect();
//         drop(monkey);
//         for throw in throws {
//             let monkey = monkeys.get_mut(throw.0).unwrap();
//             monkey.items.push_back(throw.1);
//         }
//     }
// }

fn round_p2(monkeys: &mut Vec<Monkey>) {
    for i in 0..monkeys.len() {
        let monkey = monkeys.get_mut(i).unwrap();
        let throws = monkey.inspect_p2();
        drop(monkey);
        for throw in throws {
            let monkey = monkeys.get_mut(throw.0).unwrap();
            monkey.items.push_back(throw.1);
        }
    }
}
