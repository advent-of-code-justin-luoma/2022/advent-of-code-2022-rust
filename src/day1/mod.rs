use lazy_static::lazy_static;
use regex::Regex;

use crate::read_file;

pub fn part1() {
    let text = read_file("./inputs/day1");

    let elves = split_elves(&text);

    let elves = elves
        .iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|v| v.parse().unwrap())
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();

    let calories = elves
        .iter()
        .map(|calories| calories.iter().sum())
        .collect::<Vec<u32>>();

    println!("{}", calories.iter().max().unwrap());
}

pub fn part2() {
    let text = read_file("./inputs/day1");

    let elves = split_elves(&text);

    let elves = elves
        .iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|v| v.parse().unwrap())
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();

    let mut calories = elves
        .iter()
        .map(|calories| calories.iter().sum())
        .collect::<Vec<u32>>();

    let (i1, max1) = calories
        .iter()
        .enumerate()
        .max_by(|(_, v1), (_, v2)| v1.cmp(v2))
        .unwrap();

    let max1 = *max1;

    calories.remove(i1);

    let mut calories = calories.clone();

    let (i2, max2) = calories
        .iter()
        .enumerate()
        .max_by(|(_, v1), (_, v2)| v1.cmp(v2))
        .unwrap();

    let max2 = *max2;

    calories.remove(i2);

    let (_, max3) = calories
        .iter()
        .enumerate()
        .max_by(|(_, v1), (_, v2)| v1.cmp(v2))
        .unwrap();

    println!("{:?}", max1 + max2 + max3);
}

pub fn example() {
    let file = read_file("./inputs/day1example");

    let elves = split_elves(&file);

    let elves = elves
        .iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|v| v.parse().unwrap())
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();

    let calories = elves
        .iter()
        .map(|calories| calories.iter().sum())
        .collect::<Vec<u32>>();

    println!("{:?}", elves);
    println!("{:?}", calories.iter().max());
}

fn split_elves(text: &str) -> Vec<&str> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(\r\n\r\n)").unwrap();
    }

    RE.split(text).collect::<Vec<&str>>()
}
