use std::cmp::Ordering;

use pathfinding::prelude::bfs;

use crate::read_lines;

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Pos(usize, usize);

type Grid = Vec<Vec<char>>;

pub fn part1() {
    let grid = parse_input("./inputs/day12");

    let start = find(&grid, 'S');

    let end = find(&grid, 'E');

    println!("{:?}\n{:?}", start, end);

    let result = bfs(&start, |p| next(&grid, p), |p| *p == end);

    println!("{:#?}, {}", result.clone(), result.unwrap().len());
}

pub fn part2() {
    let grid = parse_input("./inputs/day12");

    let mut starts = find_all(&grid, 'a');

    starts.push(find(&grid, 'S'));

    let end = find(&grid, 'E');

    let less = starts
        .iter()
        .map(|pos| bfs(pos, |p| next(&grid, p), |p| *p == end))
        .min_by(|route1, route2| {
            if let Some(route1) = route1 {
                if let Some(route2) = route2 {
                    route1.len().cmp(&route2.len())
                } else {
                    Ordering::Less
                }
            } else if let Some(_) = route2 {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        });

    println!("{:#?}", less.unwrap().unwrap().len());
}

pub fn example() {
    let grid = parse_input("./inputs/day12example");

    let start = find(&grid, 'S');

    let end = find(&grid, 'E');

    println!("{:?}\n{:?}", start, end);

    let result = bfs(&start, |p| next(&grid, p), |p| *p == end);

    result.clone().unwrap().iter().for_each(|pos| {
        println!("{:?}", grid[pos.1][pos.0]);
    });

    println!("{}", result.unwrap().len());

    // println!("{:#?}, {}", result.clone(), result.unwrap().len());
}

pub fn example_p2() {
    let grid = parse_input("./inputs/day12example");

    let mut starts = find_all(&grid, 'a');

    starts.push(find(&grid, 'S'));

    let end = find(&grid, 'E');

    let less = starts
        .iter()
        .map(|pos| bfs(pos, |p| next(&grid, p), |p| *p == end))
        .min_by(|route1, route2| {
            if let Some(route1) = route1 {
                if let Some(route2) = route2 {
                    route1.len().cmp(&route2.len())
                } else {
                    Ordering::Less
                }
            } else if let Some(_) = route2 {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        });

    println!("{:#?}", less.unwrap().unwrap().len());
}

fn find_all(grid: &Grid, c: char) -> Vec<Pos> {
    let mut locations = Vec::new();

    for (y, row) in grid.iter().enumerate() {
        for (x, &value) in row.iter().enumerate() {
            if c == value {
                locations.push(Pos(x, y));
            }
        }
    }

    locations
}

fn next(grid: &Grid, pos: &Pos) -> Vec<Pos> {
    let mut neighbors = Vec::new();

    let x = pos.0;
    let y = pos.1;

    // down
    if y < grid.len() - 1 {
        neighbors.push(Pos(x, y + 1));
    }

    //  left
    if x > 0 {
        neighbors.push(Pos(x - 1, y));
    }

    // up
    if y > 0 {
        neighbors.push(Pos(x, y - 1));
    }

    // right
    if x < grid[0].len() - 1 {
        neighbors.push(Pos(x + 1, y));
    }

    let elevation = grid[y][x];

    neighbors
        .iter()
        .filter(|Pos(x, y)| {
            let neighbor = grid[*y][*x];
            (elevation == 'S' && neighbor == 'a')
            || (elevation == 'S' && neighbor == 'b')
                || (elevation == 'z' && neighbor == 'E')
                || (elevation == 'y' && neighbor == 'E')
                || (elevation as u32 + 1 == neighbor as u32)
                // || elevation == neighbor
                || ((elevation as u32 >= neighbor as u32) && elevation != 'S' && neighbor != 'E')
            // current_square + 1 == neighbor
            //     || current_square == neighbor
            //     || neighbor == 'E' as u32
        })
        .map(|pos| pos.to_owned())
        .collect()
}

fn find(grid: &Grid, c: char) -> Pos {
    for (y, row) in grid.iter().enumerate() {
        if let Some((x, _)) = row.iter().enumerate().find(|(_, &value)| value == c) {
            return Pos(x, y);
        }
    }

    unreachable!()
}

fn parse_input(file: &str) -> Grid {
    let lines = read_lines(file);

    let grid: Grid = lines
        .map(|line| {
            let line = line.unwrap();
            line.chars().collect()
        })
        .collect();

    grid
}

/*
use std::collections::{HashMap, VecDeque};

// Returns a VecDeque of (row, column) pairs representing the path from the
// starting point to the destination, or None if no path exists.
fn find_path(
    heightmap: &Vec<Vec<char>>,
    start: (usize, usize),
    destination: (usize, usize),
) -> Option<VecDeque<(usize, usize)>> {
    // We use a VecDeque to store the path as we search for it.
    let mut path = VecDeque::new();

    // We also need a way to keep track of which squares we've visited.
    let mut visited = HashMap::new();

    // We'll use a queue to store the squares we need to visit.
    let mut queue = VecDeque::new();
    queue.push_front(start);

    // While there are squares in the queue...
    while !queue.is_empty() {
        // Take the first square off the queue and check if it's the destination.
        let current_square = queue.pop_front().unwrap();
        if current_square == destination {
            // If it is, we've found a path, so we can reconstruct it and return it.
            let mut current_square = current_square;
            while current_square != start {
                path.push_front(current_square);
                current_square = visited[&current_square];
            }
            path.push_front(start);
            return Some(path);
        }

        // If it's not the


*/
